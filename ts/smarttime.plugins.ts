// @pushrocks scope
import * as lik from '@push.rocks/lik';
import * as smartdelay from '@push.rocks/smartdelay';
import * as smartpromise from '@push.rocks/smartpromise';

export { lik, smartdelay, smartpromise };

// third parties;
import croner from 'croner';
import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday.js';
import prettyMs from 'pretty-ms';

dayjs.extend(isToday);

export { croner, dayjs, prettyMs };
