/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smarttime',
  version: '4.0.2',
  description: 'handle time in smart ways'
}
